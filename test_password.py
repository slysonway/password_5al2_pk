import requests

# Demander à l'utilisateur de rentrer le mot de passe à vérifier
user_password = input("Veuillez entrer le mot de passe à vérifier: \n")

# Créer une fonction pour récupérer les 500 mots de passe populaires et comparer
def worst_500_password(password):
    #Download la SecList
    print("Téléchargement de la liste des 500 mots de passe...")
    url = 'https://raw.githubusercontent.com/danielmiessler/SecLists/master/Passwords/Common-Credentials/500-worst-passwords.txt'
    retrieve = requests.get(url)
    
    content = retrieve.content

    if str(password) in str(content):
        print("Mot de passe correspondant dans les 500 pires mots de passe... Chagez-le immédiatement")
        exit(0)


# Créer une fonction pour récupérer les 10k mots de passe populaire et comprarer
def worst_10k_password(password):
    #Download la SecList
    print("Téléchargement de la liste des 10k mots de passe...")
    url = "https://raw.githubusercontent.com/danielmiessler/SecLists/master/Passwords/Common-Credentials/10k-most-common.txt"
    retrieve = requests.get(url)
    #Stocker le contenu dans la variable content
    content = retrieve.content

    if str(password) in str(content):
        print("Mot de passe correspondant dans les 10k pires mots de passe... Chagez-le immédiatement")
        exit(0)

    
if __name__ == "__main__":
    worst_500_password(user_password)
    worst_10k_password(user_password)
